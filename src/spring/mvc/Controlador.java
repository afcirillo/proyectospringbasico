package spring.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Controlador {
	
	@RequestMapping	//especificar que es el metodo encargado de mapear la vista que queremos ver
	public String muestraPagina() {
		return "paginaEjemplo";
	}

}
